﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class LogicTests
    {

        //SquirrelCigarParty
        [TestCase(30, false, false)]
        [TestCase(50, false, true)]
        [TestCase(70, true, true)]
        public void GreatPartyTest(int cigars, bool isWeekend, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.GreatParty(cigars, isWeekend);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //CanHazTable
        [TestCase(5, 10, 2)]
        [TestCase(5, 2, 0)]
        [TestCase(5,5,1)]
        public void CanHazTableTest(int yourStyle, int dateStyle, int expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            int actual = obj.CanHazTable(yourStyle,dateStyle);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //PlayOutside
        [TestCase(70, false, true)]
        [TestCase(95, false, false)]
        [TestCase(95, true, true)]
        public void PlayOutsideTest(int temp, bool isSummer, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.PlayOutside(temp, isSummer);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //CaughtSpeeding
        [TestCase(60, false, 0)]
        [TestCase(65, false, 1)]
        [TestCase(65, true, 0)]
        public void CaughtSpeedingTest(int speed, bool isBirthday,int expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            int actual = obj.CaughtSpeeding(speed, isBirthday);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //SkipSum
        [TestCase(3, 4, 7)]
        [TestCase(9, 4, 20)]
        [TestCase(10, 11, 21)]
        public void SkipSumTest(int a, int b, int expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            int actual = obj.SkipSum(a, b);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //AlarmClock
        [TestCase(1, false, "7:00")]
        [TestCase(5, false, "7:00")]
        [TestCase(0, false, "10:00")]
        public void AlarmClockTest(int day, bool vacation, string expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            string actual = obj.AlarmClock(day, vacation);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //LoveSix
        [TestCase(6, 4, true)]
        [TestCase(5, 4, false)]
        [TestCase(1, 5, true)]
        public void LoveSixTest(int a, int b, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.LoveSix(a, b);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //InRange
        [TestCase(5, false, true)]
        [TestCase(11, false, false)]
        [TestCase(11, true, true)]
        public void InRangeTest(int n, bool outsideMode, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.InRange(n, outsideMode);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Special11
        [TestCase(22, true)]
        [TestCase(23, true)]
        [TestCase(24, false)]
        public void Special11Test(int n, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.SpecialEleven(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Mod20
        [TestCase(20, false)]
        [TestCase(21, true)]
        [TestCase(22, true)]
        public void Mod20Test(int n, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.Mod20(n);

            // assert
            Assert.AreEqual(expected, actual);
        }
        //Mod35
        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(15, false)]
        public void Mod35Test(int n, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.Mod35(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //AnswerCell
        [TestCase(false, false, false, true)]
        [TestCase(false, false, true, false)]
        [TestCase(true, false, false, false)]
        public void AnswerCellTest(bool isMorning, bool isMom, bool isAsleep, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.AnswerCell(isMorning,isMom,isAsleep);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //TwoIsOne
        [TestCase(1,2,3, true)]
        [TestCase(3,1,2, true)]
        [TestCase(3,2,2, false)]
        public void TwoIsOneTest(int a, int b, int c, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.TwoIsOne(a,b,c);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //AreInOrder
        [TestCase(1, 2, 4, false, true)]
        [TestCase(1, 2, 1, false, false)]
        [TestCase(1, 1, 2, true, true)]
        public void AreInOrderTest(int a, int b, int c, bool bOk, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.AreInOrder(a, b, c, bOk);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //InOrderEqual
        [TestCase(2, 5, 11, false, true)]
        [TestCase(5, 7, 6, false, false)]
        [TestCase(5, 5, 7, true, true)]
        public void InOrderEqualTest(int a, int b, int c, bool equalbOk, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.InOrderEqual(a, b, c, equalbOk);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //LastDigit
        [TestCase(23, 19, 13, true)]
        [TestCase(23, 19, 12, false)]
        [TestCase(23, 19, 3, true)]
        public void LastDigitTest(int a, int b, int c, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.LastDigit(a, b, c);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //RollDice
        [TestCase(2, 3, true, 5)]
        [TestCase(3, 3, true, 7)]
        [TestCase(3, 3, false, 6)]
        public void RollDiceTest(int die1, int die2, bool noDoubles, int expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            int actual = obj.RollDice(die1, die2, noDoubles);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}
