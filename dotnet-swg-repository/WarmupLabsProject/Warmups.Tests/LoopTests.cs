﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class LoopTests
    {
        //StringTimes
        [TestCase("Hi", 2, "HiHi")]
        [TestCase("Hi", 3, "HiHiHi")]
        [TestCase("Hi", 1, "Hi")]
        public void StringTimesTest(string str, int n, string expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            string actual = obj.StringTimes(str, n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //FrontTimes
        [TestCase("Chocolate", 2, "ChoCho")]
        [TestCase("Chocolate", 3, "ChoChoCho")]
        [TestCase("Abc", 3, "AbcAbcAbc")]
        public void FrontTimes(string str, int n, string expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            string actual = obj.FrontTimes(str, n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //CountXX
        [TestCase("abcxx",1)]
        [TestCase("xxx", 2)]
        [TestCase("xxxx", 3)]
        public void CountXXTest(string str, int expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            int actual = obj.CountXX(str);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //DoubleX
        [TestCase("axxbb", true)]
        [TestCase("axaxxax", false)]
        [TestCase("xxxxx", true)]
        public void DoubleXTest(string str, bool expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            bool actual = obj.DoubleX(str);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //EveryOther
        [TestCase("Hello", "Hlo")]
        [TestCase("Hi", "H")]
        [TestCase("Heeololeo", "Hello")]
        public void EveryOtherTest(string str, string expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            string actual = obj.EveryOther(str);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //StringSplosion
        [TestCase("Code", "CCoCodCode")]
        [TestCase("abc", "aababc")]
        [TestCase("ab", "aab")]
        public void StringSplosionTest(string str, string expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            string actual = obj.StringSplosion(str);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //CountLast2
        [TestCase("hixxhi", 1)]
        [TestCase("xaxxaxaxx", 1)]
        [TestCase("axxxaaxx", 2)]
        public void CountLast2(string str, int expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            int actual = obj.CountLast2(str);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Count9
        [TestCase(new[] {1,2,9}, 1)]
        [TestCase(new[] {1,9,9}, 2)]
        [TestCase(new[] {1,9,9,3,9}, 3)]
        public void Count9(int[] numbers, int expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            int actual = obj.Count9(numbers);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //ArrayFront9
        [TestCase(new[] {1,2,9,3,4}, true)]
        [TestCase(new[] {1,2,3,4,9}, false)]
        [TestCase(new[] {1,2,3,4,5}, false)]
        public void ArrayFront9(int[] numbers, bool expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            bool actual = obj.ArrayFront9(numbers);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Array123
        [TestCase(new[] { 1, 1, 2, 3, 1 }, true)]
        [TestCase(new[] { 1, 1, 2, 4, 1 }, false)]
        [TestCase(new[] { 1, 1, 2, 1, 2, 3 }, true)]
        public void Array123Test(int[] numbers, bool expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            bool actual = obj.Array123(numbers);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //SubStringMatch
        [TestCase("xxcaazz", "xxbaaz", 3)]
        [TestCase("abc", "abc", 2)]
        [TestCase("abc", "axc", 0)]
        public void SubStringMatchTest(string a, string b, int expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            int actual = obj.SubStringMatch(a, b);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //StringX
        [TestCase("xxHxix", "xHix")]
        [TestCase("abxxxcd", "abcd")]
        [TestCase("xabxxxcdx", "xabcdx")]
        public void StringXTest(string str, string expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            string actual = obj.StringX(str);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //AltPairs
        [TestCase("kitten", "kien")]
        [TestCase("Chocolate", "Chole")]
        [TestCase("CodingHorror", "Congrr")]
        public void AltPairsTest(string str, string expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            string actual = obj.AltPairs(str);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //DoNotYak
        [TestCase("yakpak", "pak")]
        [TestCase("pakyak", "pak")]
        [TestCase("yak123ya", "123ya")]
        public void DoNotYak(string str, string expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            string actual = obj.DoNotYak(str);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Array667
        [TestCase(new[] { 6, 6, 2 }, 1)]
        [TestCase(new[] { 6, 6, 2, 6 }, 1)]
        [TestCase(new[] { 6, 7, 2, 6 }, 1)]
        public void Array667(int[] numbers, int expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            int actual = obj.Array667(numbers);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //NoTriples
        [TestCase(new[] { 1, 1, 2, 1 }, true)]
        [TestCase(new[] { 1, 1, 2, 2, 2, 1}, false)]
        [TestCase(new[] { 1, 1, 1, 2, 2, 2, 1 }, false)]
        public void NoTriplesTest(int[] numbers, bool expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            bool actual = obj.NoTriples(numbers);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Pattern51
        [TestCase(new[] { 1, 2, 7, 1 }, true)]
        [TestCase(new[] { 1, 2, 8, 1 }, false)]
        [TestCase(new[] { 2, 7, 1 }, true)]
        public void Pattern51Test(int[] numbers, bool expected)
        {
            // arrange
            Loops obj = new Loops();

            // act
            bool actual = obj.Pattern51(numbers);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}
