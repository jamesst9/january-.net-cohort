﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class ConditionalTests
    {
        //AreWeInTrouble
        [TestCase(true, true, true)]
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        public void AreWeInTroubleTest(bool aSmile, bool bSmile, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.AreWeInTrouble(aSmile, bSmile);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //SleepingIn
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        [TestCase(false,true, true)]
        public void SleepingInTest(bool isWeekday, bool isVacation, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.CanSleepIn(isWeekday, isVacation);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //SumDouble
        [TestCase(1, 2, 3)]
        [TestCase(3,2,5)]
        [TestCase(2,2,8)]
        public void SumDoubleTest(int a, int b, int expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            int actual = obj.SumDouble(a,b);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Diff21
        [TestCase(23, 2)]
        [TestCase(10,11)]
        [TestCase(21,0)]
        public void Diff21Test(int n, int expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            int actual = obj.Diff21(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //ParrotTrouble
        [TestCase(true, 6, true)]
        [TestCase(true, 7, false)]
        [TestCase(false, 6, false)]
        public void ParrotTroubleTest(bool isTalking, int hour, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.ParrotTrouble(isTalking, hour);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Makes10
        [TestCase(9,10, true)]
        [TestCase(9,9, false)]
        [TestCase(9,1, true)]
        public void Makes10Test(int a, int b, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.Makes10(a,b);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Near100
        [TestCase(103, true)]
        [TestCase(90, true)]
        [TestCase(89, false)]
        public void NearHundredTest(int n, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.NearHundred(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //PosNeg
        [TestCase(1, -1, false, true)]
        [TestCase(-1, 1, false, true)]
        [TestCase(-4, -5, true, true)]
        public void PosNegTest(int a, int b, bool negative, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.PosNeg(a, b, negative);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //NotString
        [TestCase("candy", "not candy")]
        [TestCase("x", "not x")]
        [TestCase("not bad", "not bad")]
        public void NotStringTest(string n, string expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            string actual = obj.NotString(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //MissingChar
        [TestCase("kitten", 1, "ktten")]
        [TestCase("kitten", 0, "itten")]
        [TestCase("kitten", 4, "kittn")]
        public void MissingCharTest(string str, int n, string expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            string actual = obj.MissingChar(str, n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //FrontBack
        [TestCase("code", "eodc")]
        [TestCase("a", "a")]
        [TestCase("ab", "ba")]
        public void FrontBackTest(string n, string expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            string actual = obj.FrontBack(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Front3
        [TestCase("Microsoft", "MicMicMic")]
        [TestCase("Chocolate", "ChoChoCho")]
        [TestCase("at", "atatat")]
        public void Front3Test(string n, string expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            string actual = obj.Front3(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //BackAround
        [TestCase("cat", "tcatt")]
        [TestCase("Hello", "oHelloo")]
        [TestCase("a", "aaa")]
        public void BackAroundTest(string n, string expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            string actual = obj.BackAround(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Multiple3or5
        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(8, false)]
        public void Multiple3or5Test(int n, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.Multiple3Or5(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //StartHi
        [TestCase("hi there", true)]
        [TestCase("hi", true)]
        [TestCase("high up", false)]
        public void StartHiTest(string n, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.StartHi(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //IcyHot
        [TestCase(120, -1,  true)]
        [TestCase(-1, 120, true)]
        [TestCase(2, 120, false)]
        public void IcyHotTest(int a, int b, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.IcyHot(a, b);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Between10and20
        [TestCase(12, 99, true)]
        [TestCase(21, 12, true)]
        [TestCase(8, 99, false)]
        public void Between10And20Test(int a, int b, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.Between10And20(a, b);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //HasTeen
        [TestCase(13, 20, 10, true)]
        [TestCase(20, 19, 10, true)]
        [TestCase(20, 10, 12, false)]
        public void HasTeenTest(int a, int b,int c, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.HasTeen(a, b, c);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //So Alone
        [TestCase(13, 99, true)]
        [TestCase(21, 19, true)]
        [TestCase(13,13, false)]
        public void SoAloneTest(int a, int b, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.SoAlone(a, b);

            // assert
            Assert.AreEqual(expected, actual);
        }
        //RemoveDel
        [TestCase("adelbc", "abc")]
        [TestCase("adelHello", "aHello")]
        [TestCase("adedbc", "adedbc")]
        public void RemoveDelTest(string n, string expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            string actual = obj.RemoveDel(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //IxStart
        [TestCase("mix snacks", true)]
        [TestCase("pix snacks", true)]
        [TestCase("piz snacks", false)]
        public void IxStartTest(string n, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.IxStart(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //StartOz
        [TestCase("ozymandias", "oz")]
        [TestCase("bzoo", "z")]
        [TestCase("oxx", "o")]
        public void StartOzTest(string n, string expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            string actual = obj.StartOz(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Max
        [TestCase(1,2,3, 3)]
        [TestCase(1,3,2, 3)]
        [TestCase(3,2,1,3)]
        public void MaxTest(int a, int b, int c, int expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            int actual = obj.Max(a, b, c);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //Closer
        [TestCase(13, 8, 8)]
        [TestCase(8, 13, 8)]
        [TestCase(13, 7, 0)]
        public void Closer(int a, int b, int expected)
        {
            // arrange
            Conditionals obj = new Conditionals();
            
            // act
            int actual = obj.Closer(a, b);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //ECount
        [TestCase("Hello", true)]
        [TestCase("Heelle", true)]
        [TestCase("Heelele", false)]
        public void ECountTest(string n, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.GotE(n);

            // assert
            Assert.AreEqual(expected, actual);
        }
        //EndUp
        [TestCase("Hello", "HeLLO")]
        [TestCase("hi there", "hi thERE")]
        [TestCase("hi", "HI")]
        public void EndUpTest(string n, string expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            string actual = obj.EndUp(n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //EveryNth
        [TestCase("Miracle", 2, "Mrce")]
        [TestCase("abcdefg", 2, "aceg")]
        [TestCase("abcdefg", 3, "adg")]
        public void EveryNthTest(string str, int n, string expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            string actual = obj.EveryNth(str, n);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}
