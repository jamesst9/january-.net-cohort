﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class StringTests
    { 
        //HelloTests
        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]
        public void SayHiTest(string name, string expected)
        {
            // arrange
            Strings obj = new Strings();

            // act
            string actual = obj.SayHi(name);

            // assert
            Assert.AreEqual(expected, actual);
        }

        //ABBATests
        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void AbbaTest(string a, string b, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act 
            string actual = obj.Abba(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //TagsTest
        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "Hello", "<i>Hello</i>")]
        [TestCase("cite", "Yay", "<cite>Yay</cite>")]
        public void TagsTest(string tag, string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.MakeTags(tag, word);

            //assert
            Assert.AreEqual(expected,actual);
        }

        //InsertWordTest
        [TestCase("<<>>", "Yay", "<<Yay>>")]
        [TestCase("<<>>", "WooHoo", "<<WooHoo>>")]
        [TestCase("[[]]", "word", "[[word]]")]
        public void InsertWordTest(string container, string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.InsertWord(container, word);

            //assert
            Assert.AreEqual(expected,actual);
        }

        //MultipleEndings
        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]
        public void MultipleEndings (string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.MultipleEndings(word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //FirstHalf
        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void FirstHalfTest(string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.FirstHalf(word);

            //assert
            Assert.AreEqual(expected, actual);
        }
        //TrimOne
        [TestCase("Hello", "ello")]
        [TestCase("java", "av")]
        [TestCase("coding", "odin")]
        public void TrimOneTest(string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.TrimOne(word);

            //assert
            Assert.AreEqual(expected, actual);
        }
        //LongInMiddle
        [TestCase("Hello", "hi", "hiHellohi")]
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "b", "baaab")]
        public void LongInMiddleTest(string a,string b, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.LongInMiddle(a,b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //RotateLeft2
        [TestCase("Hello", "lloHe")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateLeft2(string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.Rotateleft2(word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //RotateRight2
        [TestCase("Hello", "loHel")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateRight2(string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.RotateRight2(word);

            //assert
            Assert.AreEqual(expected, actual);
        }
        //FromFront
        [TestCase("Hello", true, "H")]
        [TestCase("Hello", false,"o")]
        [TestCase("oh", true,"o")]
        public void FromFront(string word,bool fromFront, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.TakeOne(word, fromFront);

            //assert
            Assert.AreEqual(expected, actual);
        }
        //Middle2
        [TestCase("string", "ri")]
        [TestCase("code", "od")]
        [TestCase("Practice", "ct")]
        public void Middle2(string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.MiddleTwo(word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //wordswithLY
        [TestCase("oddly", true)]
        [TestCase("y", false)]
        [TestCase("oddy", false)]
        public void Wordwithly(string word, bool expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            bool actual = obj.EndsWithLy(word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //FrontAndBack
        [TestCase("Hello", 2, "Helo")]
        [TestCase("Chocolate", 3, "Choate")]
        [TestCase("Chocolate", 1, "Ce")]
        public void FrontAndBack(string word, int n, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.FrontAndBack(word, n);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //TakeTwoFromPosition
        [TestCase("java", 0, "ja")]
        [TestCase("java", 2, "va")]
        [TestCase("java", 3, "ja")]
        public void TakeTwoFromPosition(string word, int n, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.TakeTwoFromPosition(word, n);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //HasBad
        [TestCase("badxx", true)]
        [TestCase("xbadxx", true)]
        [TestCase("xxbad", false)]
        public void HasBad(string word, bool expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            bool actual = obj.HasBad(word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //AtFirst
        [TestCase("hello", "he")]
        [TestCase("he", "he")]
        [TestCase("h", "h@")]
        public void AtFirst(string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.AtFirst(word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //LastChars
        [TestCase("last", "chars", "ls")]
        [TestCase("yo", "mama", "ya")]
        [TestCase("hi", "", "h@")]
        public void LastChars(string a, string b, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.FirstAndLastChars(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //ConCat
        [TestCase("abc", "cat", "abcat")]
        [TestCase("dog", "cat", "dogcat")]
        [TestCase("abc", "", "abc")]
        public void ConCat(string a, string b, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.ConCat(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //SwapLast
        [TestCase("coding", "codign")]
        [TestCase("cat", "cta")]
        [TestCase("ab", "ba")]
        public void SwapLast(string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.SwapLast(word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //FrontAgain
        [TestCase("edited", true)]
        [TestCase("edit", false)]
        [TestCase("ed", true)]
        public void FrontAgain(string word, bool expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            bool actual = obj.FrontAgain(word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //MinCat
        [TestCase("Hello", "Hi", "loHi")]
        [TestCase("Hello", "java", "ellojava")]
        [TestCase("java", "Hello", "javaello")]
        public void MinCat(string a, string b, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.MinCat(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //SwapLast
        [TestCase("Hello", "llo")]
        [TestCase("away", "aay")]
        [TestCase("abed", "abed")]
        public void TweakFront(string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.TweakFront(word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        //StripX
        [TestCase("xHix", "Hi")]
        [TestCase("xHi", "Hi")]
        [TestCase("Hxix", "Hxi")]
        public void StripX(string word, string expected)
        {
            //arrange
            Strings obj = new Strings();

            //act
            string actual = obj.StripX(word);

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
