﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Warmups.BLL
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class Loops
    {
        /* Given a string and a non-negative int n, return a 
           larger string that is n copies of the original string. 
        */
        public string StringTimes(string str, int n)
        {
            string result = "";

            for (int i = 1; i <= n; i++)
            {
                result += str;
            }

            return result;
        }

        public string FrontTimes(string str, int n)
        {
            string result = "";
            string shortstr = str.Substring(0, 3);

            for (int i = 1; i <= n; i++)
            {
                result += shortstr;
            }
            return result;
        }

        public int CountXX(string word)
        {
            int xx_counter = 0;

            for (int i = 0; i < word.Length - 1; i++)
            {
                if (word.Substring(i, 1) + word.Substring(i + 1, 1) == "xx")
                {
                    xx_counter += 1;
                }
            }
            return xx_counter;
        }

        public bool DoubleX(string str)
        {
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.Substring(i, 1) == "x")
                {
                    if (str.Substring(i + 1, 1) == "x")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    continue;
                }
            }
            return false;
        }


        public string EveryOther(string str)
        {
            string newword = "";
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (i % 2 == 0)
                {
                    newword += str.Substring(i, 1);
                }
                else
                {
                    continue;
                }
            }
            return newword;
        }

        public string StringSplosion(string str)
        {
            string newword = "";
            for (int i = 0; i < str.Length - 1; i++)
            {
                newword += str.Substring(0, i);
            }
            return newword;
        }

        public int CountLast2(string str)
        {
            int times = 0;
            string last2 = str.Substring(str.Length - 2, 2);
            for (int i = 0; i < str.Length - 2; i++)
            {
                if (str.Substring(i, 2) == last2)
                {
                    times += 1;
                }
                else
                {
                    continue;
                }
            }
            return times;

        }
        public int Count9(int[] numbers)
        {
            int times = 0;
            foreach (int element in numbers)
            {
                if (element == 9)
                {
                    times += 1;
                }
                else
                {
                    continue;
                }
            }
            return times;
        }
        public bool ArrayFront9(int[] numbers)
        {
            int instance = 0;
            foreach (int element in numbers)
            {
                instance += 1;
                if (element == 9)
                {
                    if (instance > 4)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {

                    continue;
                }
            }
            return false;
        }

        public bool Array123(int[] numbers)
        {
            try
            {

                for (int i = 0; i <= numbers.Length; i++)
                {
                    if (numbers[i] == 1)
                    {
                        if (numbers[i + 1] == 2)
                        {
                            if (numbers[i + 2] == 3)
                            {
                                return true;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                return false;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }

        public int SubStringMatch(string a, string b)
        {
            int matches = 0;

            for (int i = 0; i < b.Length - 1; i++)
            {
                if (a.Substring(i, 1) == b.Substring(i, 1))
                {
                    if (a.Substring(i + 1, 1) == b.Substring(i + 1, 1))
                    {
                        matches += 1;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    continue;
                }
            }
            return matches;
        }

        public string StringX(string str)
        {
            var xRemove = new string[] { "x" };
            if (str.Substring(0, 1) == "x")
            {
                if (str.Substring(str.Length - 1, 1) == "x")
                {
                    foreach (var c in xRemove)
                    {
                        str = str.Replace(c, string.Empty);
                    }
                    return "x" + str + "x";
                }
                else
                {
                    foreach (var c in xRemove)
                    {
                        str = str.Replace(c, string.Empty);
                    }
                    return "x" + str;
                }
            }
            else
            {
                foreach (var c in xRemove)
                {
                    str = str.Replace(c, string.Empty);
                }
                return str;
            }
        }

        public string AltPairs(string str)
        {
            string newword = "";
            for (int i = 0; i <= str.Length - 1; i += 4)
            {
                if (str.Length - 1== i)
                {
                    return newword + str.Substring(str.Length - 1, 1);
                }
                newword += str.Substring(i, 2);
            }
            return newword;
        }

        public string DoNotYak(string str)
        {
            return str.Replace("yak", "");
        }

        public int Array667(int[] numbers)
        {
            int times = 0;
            for(int i=0;i < numbers.Length - 1;i++)
            {
                if (numbers[i] == 6 && numbers[i + 1] == 6 || numbers[i + 1] == 7)
                {
                    times += 1;
                }
                else
                {
                    continue;
                }
            }
            return times;
        }

        public bool NoTriples(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == numbers[i + 1] && numbers[i] == numbers[i + 2])
                {
                    return false;
                }
                else
                {
                    continue;
                }
            }
            return true;
        }

        public bool Pattern51(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == 2)
                {
                    if (numbers[i + 1] == 7)
                    {
                        if (numbers[i + 2] == 1)
                        {
                            return true;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    continue;
                }
            }
            return false;
        }
    }
}

