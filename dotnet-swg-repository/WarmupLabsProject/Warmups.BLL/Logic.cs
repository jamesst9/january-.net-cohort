﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Logic
    {
        /* When squirrels get together for a party, they like to have cigars. 
           A squirrel party is successful when the number of cigars is between 
           40 and 60, inclusive. Unless it is the weekend, in which case there is 
           no upper bound on the number of cigars. Return true if the party with 
           the given values is successful, or false otherwise. 
        */
        public bool GreatParty(int cigars, bool isWeekend)
        {
            if (isWeekend)
                return cigars > 40;
            else
                return (cigars >= 40 && cigars <= 60);
        }

        public int CanHazTable(int yourStyle, int dateStyle)
        {
            if (yourStyle >= 8 || dateStyle >= 8)
            {
                return 2;
            }
            else if (yourStyle <= 2 || dateStyle <= 2)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public bool PlayOutside(int temp, bool isSummer)
        {
            if (isSummer)
            {
                if (temp > 60 && temp < 100)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (temp > 60 && temp < 90)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public int CaughtSpeeding(int speed, bool isBirthday)
        {
            if (isBirthday)
            {
                if (speed >= 66 && speed <= 85)
                {
                    return 1;
                }
                else if (speed >= 86)
                {
                    return 2;
                }
                else
                {
                    return 0;
                }

            }
            else
            {
                if (speed >= 61 && speed <= 80)
                {
                    return 1;
                }
                else if (speed >= 81)
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int SkipSum(int a, int b)
        {
            if (a + b >= 10 && a + b <= 19)
            {
                return 20;
            }
            else
            {
                return a + b;
            }
        }


        public string AlarmClock(int day, bool vacation)
        {
            if (vacation)
            {
                if (day == 0 || day == 6)
                {
                    return "off";
                }
                else
                {
                    return "10:00";
                }
            }
            else
            {
                if (day == 0 || day == 6)
                {
                    return "10:00";
                }
                else
                {
                    return "7:00";
                }
            }

        }

        public bool LoveSix(int a, int b)
        {
            if (a == 6 || b == 6 || a + b == 6 || b - a == 6 || a - b == 6)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool InRange(int n, bool outsideMode)
        {
            if (outsideMode)
            {
                if (n < 1 || n > 10)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (n >= 1 && n <= 10)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool SpecialEleven(int n)
        {
            if (n%11 == 0 || (n - 1)%11 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Mod20(int n)
        {
            if (n%20 == 1 || n%20 == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Mod35(int n)
        {
            if (n%3 == 0 && n%5 == 0)
            {
                return false;
            }
            else if (n%3 == 0 || n%5 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AnswerCell(bool isMorning, bool isMom, bool isAsleep)
        {
            if (isAsleep)
            {
                return false;
            }
            else
            {
                if (isMorning)
                {
                    if (isMom)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        public bool TwoIsOne(int a, int b, int c)
        {
            if (a + b == c || b + c == a || c + a == b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AreInOrder(int a, int b, int c, bool bOk)
        {
            if (bOk)
            {
                if (c > b)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (b > a && c > b)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool InOrderEqual(int a, int b, int c, bool equalOk)
        {
            if (equalOk)
            {
                if (a < b && b < c || a == b || b == c)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (b > a && c > b)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool LastDigit(int a, int b, int c)
        {
            if (a%10 == b%10 || b%10 == c%10 || a%10 == c%10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int RollDice(int die1, int die2, bool noDoubles)
        {
            if (noDoubles)
            {
                if (die1 == die2)
                {
                    return die1 + die2 + 1;
                }
                else
                {
                    return die1 + die2;
                }
            }
            else
            {
                return die1 + die2;
            }
        }
    }
}
