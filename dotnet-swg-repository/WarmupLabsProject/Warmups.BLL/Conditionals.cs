﻿using System;
using System.Linq;

namespace Warmups.BLL
{
    public class Conditionals
    {
        /* We have two children, a and b, and the parameters aSmile and 
           bSmile indicate if each is smiling. We are in trouble if they 
           are both smiling or if neither of them is smiling. Return true 
           if we are in trouble. 
        */
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if (aSmile && bSmile)
                return true;

            if (!aSmile && !bSmile)
                return true;

            return false;
        }

        public bool CanSleepIn(bool isWeekday, bool isVacation)
        {
            if (isWeekday == false || isVacation)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int SumDouble(int a, int b)
        {
            if (a == b)
            {
                return 2*(a + b);
            }
            else
            {
                return a + b;
            }
        }

        public int Diff21(int n)
        {
            return Math.Abs(n-21);
        }

        public bool ParrotTrouble(bool isTalking, int hour)
        {
            if (isTalking)
            {
                if (hour < 7 || hour > 20)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool Makes10(int a, int b)
        {
            if (a == 10 || b == 10 || a + b == 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool NearHundred(int n)
        {
            if (90 <= n && n <= 110 || 190 <= n && n <= 210)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool PosNeg(int a, int b, bool negative)
        {
            if (negative)
            {
                if (a < 0 && b < 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (a < 0 && b > 0 || a > 0 && b < 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string NotString(string s)
        {
            if (s.Length < 3)
            {
                return "not " + s;
            }

            else if (s.Substring(0, 3) == "not")
            {
                return s;
            }
            else
            {
                return "not " + s;
            }
        }

        public string MissingChar(string str, int n)
        {
            return str.Replace(str.Substring(n,1), "");
        }

        public string FrontBack(string str)
        {
            if (str.Length == 1)
            {
                return str;
            }
            else
            {
                return str.Substring(str.Length - 1, 1) + str.Substring(1, str.Length - 2)+str.Substring(0,1);
            }
        }

        public string Front3(string str)
        {
            if (str.Length < 3)
            {
                return str + str + str;
            }
            else
            {
                return str.Substring(0, 3) + str.Substring(0, 3) + str.Substring(0, 3);
            }
        }

        public string BackAround(string str)
        {
            return str.Substring(str.Length - 1, 1) + str + str.Substring(str.Length - 1, 1);
        }

        public bool Multiple3Or5(int number)
        {
            if (number%3 == 0 || number%5 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool StartHi(string str)
        {
            string[] words = str.Split(' ');
            if (words[0] == "hi")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IcyHot(int temp1, int temp2)
        {
            if (temp1 > 100 && temp2 < 0 || temp2 > 100 && temp1 < 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Between10And20(int a, int b)
        {
            if (a >= 10 && a <= 20 || b >= 10 && b <= 20)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool HasTeen(int a, int b, int c)
        {
            if (a >= 13 && a <= 19 || b >= 13 && b <= 19 || c >= 13 && c <= 19)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SoAlone(int a, int b)
        {
            if (a >= 13 && a <= 19 && b >= 13 && b <= 19)
            {
                return false;
            }
            else if (a >= 13 && a <= 19 || b >= 13 && b <= 19)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string RemoveDel(string str)
        {
            return str.Replace("del", "");
        }

        public bool IxStart(string str)
        {
            if (str.Substring(1, 2) == "ix")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string StartOz(string str)
        {
            if (str.Substring(0, 1) == "o")
            {
                if (str.Substring(1, 1) == "z")
                {
                    return str.Substring(0, 2);
                }
                else
                {
                    return str.Substring(0, 1);
                }
            }
            else if (str.Substring(1,1) == "z")
            {
                return str.Substring(1, 1);
            }
            else
            {
                return "";
            }
        }

        public int Max(int a, int b, int c)
        {
            int[] numbers = {a, b, c};
            return numbers.Max();
        }

        public int Closer(int a, int b)
        {
            int aclose = Math.Abs(10 - a);
            int bclose = Math.Abs(10 - b);

            if (aclose < bclose)
            {
                return a;
            }
            else if (bclose < aclose)
            {
                return b;
            }
            else
            {
                return 0;
            }
        }

        public bool GotE(string str)
        {
            int eCounter = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str.Substring(i, 1) == "e")
                {
                    eCounter += 1;
                }
            }
            if (eCounter <= 3)
            {
                return true;
            }
            return false;
        }
        public string EndUp(string str)
        {
            if (str.Length < 3)
            {
                return str.ToUpper();
            }
            else
            {
                return str.Substring(0, str.Length - 3) + str.Substring(str.Length - 3, 3).ToUpper();
            }
        }
        public string EveryNth(string str, int n)
        {
            string newword = "";
            for (int i = 0; i*n < str.Length; i++)
            {
                newword += str.Substring(i*n, 1);
            }
            return newword;
        }
    }
}
