﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Arrays
    {
        /* Given an array of ints, return true if 6 appears 
           as either the first or last element in the array. 
           The array will be length 1 or more. 
        */
        public bool FirstLast6(int[] numbers)
        {
            // 0 is always the first index and 
            // Length - 1 of an array is always the last index
            return (numbers[0] == 6 || numbers[numbers.Length - 1] == 6);
        }

        public bool SameFirstLast(int[] numbers)
        {
            return (numbers.Length > 1 && numbers[0] == numbers[numbers.Length - 1]);
        }

        public int[] MakePi(int n)
        {
            string piHolder = Math.PI.ToString().Replace(".","");
            int[] piHolderInts = new int[n];
            for(int i = 0; i < n ; i++)
            {
                piHolderInts[i] = int.Parse(piHolder[i].ToString()); // Convert.ToInt32(piHolder[i]);
            }
            return piHolderInts;
        }

        public bool commonEnd(int[] a, int[] b)
        {
            if (a[0] == b[0] || a[a.Length - 1] == b[b.Length - 1])
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int Sum(int[] numbers)
        {
            int arraySum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                arraySum += numbers[i];
            }
            return arraySum;
        }

        public int[] RotateLeft(int[] numbers)
        {
            int[] rotato = new int[numbers.Length];
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers.Length - i == 1)
                {
                    rotato[i] = numbers[0];
                    return rotato;
                }
                rotato[i] = numbers[i + 1];

            }
            return rotato;
        }

        public int[] Reverse(int[] numbers)
        {
            int[] newArray = new int[numbers.Length];
            for (int i = 0; i < numbers.Length ; i++)
            {
                newArray[i] = numbers[numbers.Length - i - 1];
            }
            return newArray;
        }


        public int[] HigherWins(int[] numbers)
        {
            int highNum = Math.Max(numbers[0],numbers[numbers.Length-1]);
            int[] newArray = new int[numbers.Length];
            
            for (int i = 0; i < numbers.Length ; i++)
            {
                newArray[i] = highNum;
            }
            return newArray;
        }

        public int[] GetMiddle(int[] a, int[] b)
        {
            int[] newArray = new int[2];
            newArray[0] = a[a.Length/2];
            newArray[1] = b[b.Length/2];
            return newArray;
        }

        public bool HasEven(int[] numbers)
        {
            int i = 0;
            do
            {
                if (numbers[i]%2 == 0)
                {
                    return true;
                }
                else
                {
                    i++;
                }
            } while (i < numbers.Length);
            return false;
        }


        public int[] KeepLast(int[] numbers)
        {
            int[] newArray = new int[numbers.Length*2];
            newArray[newArray.Length - 1] = numbers[numbers.Length - 1];
            return newArray;
        }


        public bool Double23(int[] numbers)
        {
            int twocounter = 0;
            int threecounter = 0;
            int i = 0;
            do
            {
                if (numbers[i] == 2)
                {
                    twocounter++;
                }
                else if (numbers[i] == 3)
                {
                    threecounter++;
                }
                i++;
            } while (i < numbers.Length);
            if (twocounter == 2 || threecounter == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int[] Fix23(int[] numbers)
        {
            int i = 0;
            do
            {
                try
                {
                    if (numbers[i] == 2)
                    {
                        if (numbers[i + 1] == 3)
                        {
                            numbers[i + 1] = 0;
                        }
                        else
                        {
                            i++;
                        }
                    }
                    else
                    {
                        i++;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    i++;
                    continue;
                }
            } while (i < numbers.Length);
            return numbers;
        }

        public bool Unlucky1(int[] numbers)
        {
            int i = 0;
            do
            {
                try
                {
                    if (numbers[i] == 1)
                    {
                        if (numbers[i + 1] == 3)
                        {
                            return true;
                        }
                        else
                        {
                            i++;
                            continue;
                        }
                    }
                    else
                    {
                        i++;
                        continue;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    i++;
                    continue;
                }
            } while (i < numbers.Length);
            return false;
        }

        public int[] Make2(int[] a, int[] b)
        {
            int[] newArray = new int[2];
            int i = 0;
            int n = 0;
            do
            {
                try
                {
                    newArray[i] = a[i];
                    i++;
                }
                catch (IndexOutOfRangeException)
                {
                    
                    newArray[i] = b[n];
                    n++;
                    i++;
                }
            } while (i < newArray.Length);
            return newArray;
        }
    }
}
