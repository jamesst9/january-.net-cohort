﻿using System;

namespace Warmups.BLL
{
    public class Strings
    {
        // Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!". 
        public string SayHi(string name)
        {
            return string.Format("Hello {0}!", name);
        }

        // Abba
        public string Abba(string a, string b)
        {
            return string.Format("{0}{1}{1}{0}", a, b);
        }

        //MakeTags
        public string MakeTags(string tag, string word)
        {
            return string.Format("<{0}>{1}</{0}>",tag, word);
        }

        //InsertWord
        public string InsertWord(string container, string word)
        {
            string left = container.Substring(0, 2);
            string right = container.Substring(2, 2);

            return string.Format("{0}{1}{2}",left,word,right);
        }

        //MultipleEndings
        public string MultipleEndings(string word)
        {
            if (word.Length <= 2)
            {
                return string.Format("{0}{0}{0}", word);
            }
            string lasttwo = word.Substring(word.Length - 2);
            return string.Format("{0}{0}{0}", lasttwo);

        }
        //FirstHalf
        public string FirstHalf(string word)
        {
            string half = word.Substring(0,word.Length/2);
            return half;
        }

        //TrimOne
        public string TrimOne(string word)
        {
            if (word.Length%2 == 1)
            {
                string trimmed = word.Substring(1, word.Length - 1);
                return trimmed;
            }
            else
            {
                string trimmed = word.Substring(1, word.Length - 2);
                return trimmed;
            }
          
        }
        //LongInMiddle

        public string LongInMiddle(string a, string b)
        {
            if (a.Length > b.Length)
            {
                return string.Format("{0}{1}{0}", b, a);
            }
            else
            {
                return string.Format("{0}{1}{0}", a, b);
            }
            
        }
        //RotateLeft2
        public string Rotateleft2(string word)
        {
            if (word.Length <= 2)
            {
                return word;
            }
            else
            {
                string rotateword = word.Substring(2, word.Length - 2) + word.Substring(0, 2);
                return rotateword;
            }
        }

        //RotateRight2
         public string RotateRight2(string word)
            {
             if (word.Length <= 2)
             {
                 return word;

             }
             else
             {
                 string rotateword = word.Substring(word.Length - 2, 2) + word.Substring(0, word.Length - 2);
                 return rotateword;

             }

            }
        public string TakeOne(string word, bool fromFront)
        {
            if (fromFront == false)
            {
                return word.Substring(word.Length-1);
            }
            else
            {
                return word.Substring(0,1);
            }
        }

        public string MiddleTwo(string word)
        {
            return word.Substring((word.Length/2) - 1, 2);
        }
        public bool EndsWithLy(string word)
        {
            if (word.Length < 2)
            {
                return false;
            }
            else if (word.Substring(word.Length - 2, 2) == "ly")
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public string FrontAndBack(string word, int n)
        {
            string newword = word.Substring(0, n) + word.Substring(word.Length - n, n);
            return newword;
        }


        public string TakeTwoFromPosition(string word, int n)
        {
            try
            {
                return word.Substring(n, 2);
            }
            catch (ArgumentOutOfRangeException)
            {
                return word.Substring(0, 2);
            }
        }

        public bool HasBad(string word)
        {
            if (word.Substring(0, 3) == "bad" || word.Substring(1, 3) == "bad")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string AtFirst(string word)
        {
            if (word.Length < 2)
            {
                return word + "@";
            }
            else
            {
                return word.Substring(0, 2);
            }
        }

        public string FirstAndLastChars(string a, string b)
        {
            if (a.Length == 0)
            {
                return "@" + b.Substring(b.Length - 1, 1);
            }
            else if (b.Length == 0)
            {
                return a.Substring(0, 1) + "@";
            }
            else
            {
                return a.Substring(0, 1) + b.Substring(b.Length - 1, 1);
            }
        }


        public string ConCat(string a, string b)
        {
            try
            {
                if (a.Substring(a.Length - 1, 1) == b.Substring(0, 1))
                {
                    return a.Substring(0, a.Length - 1) + b.Substring(0, b.Length);
                }

                else
                {
                    return a + b;
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                return a + b;
            }
           
        }

        public string SwapLast(string word)
        {
            return word.Substring(0, word.Length - 2) + word.Substring(word.Length - 1, 1)+ word.Substring(word.Length - 2, 1);
                            
        }

        public bool FrontAgain(string word)
        {
            if (word.Substring(0, 2) == word.Substring(word.Length - 2, 2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string MinCat(string a, string b)
        {
            if (a.Length > b.Length)
            {
                return a.Substring(a.Length-b.Length, b.Length) + b;
            }
            else if (a.Length < b.Length)
            {
                return a + b.Substring(b.Length - a.Length, a.Length);
            }
            else
            {
                return a + b;
            }
        }

        public string TweakFront(string word)
        {
            if (word[0] == 'a')
            {
                if (word[1] == 'b')
                {
                    return word;
                }
                else
                {
                    return word.Substring(0, 1) + word.Substring(2, word.Length - 2);
                }
            }
            else if (word[1] == 'b')
            {
                return word.Substring(1, word.Length - 1);
            }
            else
            {
                return word.Substring(2, word.Length - 2);
            }
        }

        public string StripX(string word)
        {
            if (word[0] == 'x' && word[word.Length-1] == 'x')
            {
                return word.Substring(1, word.Length - 2);
            }
            else if (word[0] == 'x')
            {
                return word.Substring(1, word.Length - 1);
            }
            else if (word[word.Length-1] == 'x')
            {
                return word.Substring(0, word.Length - 1);
            }
            else
            {
                return word;
            }
        }
    }
    }

