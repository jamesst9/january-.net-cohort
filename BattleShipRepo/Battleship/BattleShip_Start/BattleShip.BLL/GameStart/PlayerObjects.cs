﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;

namespace BattleShip.BLL.GameStart
{
    public class PlayerObjects
    {
        public string Player1Name { get; set; }
        public string Player2Name { get; set; }
        public Board Player1Board { get; set; }
        public Board Player2Board { get; set; }
    }
}
